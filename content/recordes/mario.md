---
title: "Super Mario"
date: 2021-09-18T23:27:57-03:00
draft: false
---
## Recordes:

&nbsp;

&nbsp;

&nbsp;

-Super Mario World - 1h21m43s O recorde mundial da categoria 96 Exit, que exige encontrar todas as 96 saídas do jogo. O jogador Lui usa movimentos precisos, saltos arriscados e glitches como o cloud glitch para voar por cima das fases.

&nbsp;

&nbsp;

&nbsp;

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQY9eVPVXx4E3-REBMaBnyl5tSohST57tJ6GfJVNIhMqNjf6c-e-y5MWRqMpFJ1tcPcheI&usqp=CAU)

&nbsp;

&nbsp;

&nbsp;

-Super Mario Bros. - 4m55s913ms O recorde mundial da categoria Any%, que exige terminar o jogo o mais rápido possível. O jogador Niftski usa truques como o wall jump e o flagpole glitch para ganhar alguns frames preciosos.
&nbsp;

&nbsp;

&nbsp;

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8oJmvviy0Ng7zdOFsq_H4sjhI1KKeiifqYrnK5Ns2h5x1JwXJYy_1HrGzCSL6nTi2_TE&usqp=CAU)

&nbsp;

&nbsp;

&nbsp;

-Super Mario 64 - 6m31s O recorde mundial da categoria 0 Star, que exige terminar o jogo sem coletar nenhuma estrela. O jogador Liam usa glitches e manipulações para acessar áreas que normalmente exigiriam estrelas. Ele também usa o BLJ (Backwards Long Jump), que permite ganhar velocidade infinita e atravessar paredes.

&nbsp;

&nbsp;

&nbsp;

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdRibbJmxTiHy-clSYU_kLsB5pruQr44qzYOWbeXad6uSMraQvSrkDLd9qnJs8xdAIiTY&usqp=CAU)

&nbsp;

&nbsp;

&nbsp;

-Super Mario Odyssey - 58m47s O recorde mundial da categoria Any%, que exige terminar o jogo com o mínimo de luas possível. O jogador NicroVeda usa o chapéu do Mario para capturar vários inimigos e objetos e usá-los para 

&nbsp;

&nbsp;

&nbsp;

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEmNm7FL_gHAI1qFedNz-JtxWPpZ_uakpr_Gsbf9WP7HrmQkX4Pnp2X4PhzzER6gc0cl8&usqp=CAU)

&nbsp;

&nbsp;

&nbsp;

-Super Mario Galaxy - 2h28m06s O recorde mundial da categoria Any%, que exige terminar o jogo com o mínimo de estrelas possível. O jogador Valu usa o controle de movimento do Wii para controlar o Mario com precisão e fluidez. Ele também usa alguns truques e glitches, como o star bit storage e o bubble breeze skip, que permitem economizar tempo e energia.

&nbsp;

&nbsp;

&nbsp;

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyitN1wCSZRQgU4dy8ZVxAaZ4atR5f7tRSe2ccrplfcZ0aXKEIYHPQcOwlRRyLLHPm1L0&usqp=CAU)

