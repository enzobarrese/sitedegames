---
title: "Minecraft"
date: 2021-09-18T23:27:57-03:00
draft: false
---

## Recordes:

&nbsp;

&nbsp;

&nbsp;

Minecraft é um jogo que permite muita criatividade e liberdade, mas também pode ser um desafio para quem quer completá-lo o mais rápido possível. Existem vários tipos de speedruns de Minecraft, cada um com suas próprias regras e dificuldades. Neste artigo, vamos apresentar as 5 recordes de melhores speedruns de Minecraft, cada uma de um modo diferente. Confira!
Speedrun normal: Este é o modo mais comum e básico de speedrun, onde o objetivo é derrotar o dragão Ender no menor tempo possível, sem usar nenhum tipo de trapaça ou glitch. O recorde mundial atual nesta categoria é de 14 minutos e 56 segundos, feito pelo jogador Korbanoes em 25 de jun. de 2021.

&nbsp;

&nbsp;

&nbsp;

-Speedrun com seed não aleatória: Neste modo, o jogador pode escolher a seed do mundo, ou seja, o mapa é pré-definido e o jogador sabe onde encontrar os recursos necessários. O recorde mundial atual nesta categoria é de 1 minuto e 49 segundos, feito pelo jogador EmpireKills702 em 11 de dezembro de 2022.

&nbsp;

&nbsp;

&nbsp;

-Speedrun com glitches: Neste modo, o jogador pode usar qualquer tipo de bug ou falha do jogo para ganhar vantagem, como por exemplo, duplicar itens, atravessar paredes ou teletransportar-se. O recorde mundial atual nesta categoria é de 2 minutos e 31 segundos, feito pelo jogador Dowsky em 15 de dezembro de 2020.
&nbsp;

&nbsp;

&nbsp;

-Speedrun randomizado: Neste modo, todos os itens do jogo são trocados por outros aleatoriamente, ou seja, o jogador nunca sabe o que vai encontrar em cada baú, bloco ou criatura. O recorde mundial atual nesta categoria é de 23 minutos e 53 segundos, feito pelo jogador Fundy em 29 de agosto de 2020.

&nbsp;

&nbsp;

&nbsp;

-Speedrun hardcore: Neste modo, o jogador tem apenas uma vida e não pode regenerar sua saúde, ou seja, qualquer dano pode ser fatal. Além disso, o jogo está na dificuldade mais alta, o que torna os inimigos mais fortes e agressivos. O recorde mundial atual nesta categoria é de 19 minutos e 54 segundos, feito pelo jogador Dream em 16 de outubro de 2020.