---
title: "Marvel’s Spider-Man 2 - 20 outubro"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Marvel’s Spider-Man 2 é o novo jogo da Insomniac Games que traz de volta os heróis Peter Parker e Miles Morales em uma aventura cheia de ação, emoção e surpresas. O jogo, que será lançado exclusivamente para PlayStation 5 no dia 20 de outubro de 2023, apresenta um novo desafio para os Spiders: enfrentar o temível Venom, um dos maiores inimigos do Homem-Aranha. Além disso, o jogo também conta com a presença de outros vilões clássicos da Marvel, como Kraven The Hunter e Lizard. Marvel’s Spider-Man 2 promete ser uma experiência incrível para os fãs do cabeça de teia, com gráficos impressionantes, jogabilidade dinâmica e uma história envolvente. O jogo também aproveita os recursos do console PS5, como a resposta tátil, os gatilhos adaptáveis e o SSD de velocidade ultra-alta. Quem comprar o jogo na pré-venda receberá itens digitais exclusivos, como trajes, armas e um livro digital com a história de Spider-Man. Para saber mais sobre o jogo, visite o [site oficial](https://www.playstation.com/pt-br/games/marvels-spider-man-2/) ou assista ao [trailer](https://www.youtube.com/watch?v=07bcyesxbrg).
&nbsp;
&nbsp;
&nbsp;
![](https://artcetera.art/wp-content/uploads/2023/03/Most-Anticipated-Games-of-2023.jpg)