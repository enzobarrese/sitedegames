---
title: "Assassin’s Creed Mirage - 5 outubro"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Assassin’s Creed Mirage é o novo jogo da franquia Assassin’s Creed, que leva os jogadores para a Bagdá do século IX, uma cidade cheia de mistérios, intrigas e perigos. O jogo, que será lançado em 5 de outubro de 2023 para Xbox Series X|S, Xbox One, PlayStation®5, PlayStation®4, EPIC, PC e Amazon Luna, conta a história de Basim, um astuto ladrão de rua com visões aterrorizantes e que está em busca de respostas e de justiça. Basim se junta aos Ocultos, uma organização antiga e secreta que segue um credo diferente dos Assassinos tradicionais. Basim terá que usar suas habilidades de furtividade, combate e parkour para enfrentar os inimigos que ameaçam a paz e a liberdade da cidade. Assassin’s Creed Mirage promete ser uma experiência imersiva e envolvente, com gráficos impressionantes, jogabilidade dinâmica e uma trilha sonora épica. O jogo também conta com um modo online cooperativo, onde os jogadores podem se unir com seus amigos para realizar missões e explorar o mundo aberto.. Para saber mais sobre o jogo, visite o [site oficial](https://www.ubisoft.com/pt-br/game/assassins-creed/mirage) ou assista ao [trailer](https://www.ubisoft.com/pt-br/game/assassins-creed/mirage) de história.
&nbsp;
&nbsp;
&nbsp;
![](https://www.adrenaline.com.br/wp-content/plugins/seox-image-magick/imagick_convert.php?width=910&height=568&format=webp&quality=91&imagick=uploads.adrenaline.com.br/2023/07/basim-assassin-creed-mirage-912x569.jpg)