---
title: "Forza Motorsport - 10 outubro"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Forza Motorsport é o mais novo jogo de corrida da série Forza, desenvolvido pela Turn 10 Studios, que traz uma experiência incrível para os fãs de automobilismo. O jogo, que será lançado em 10 de outubro de 2023 para Xbox Series X|S e PC, conta com mais de 500 carros, incluindo os modelos de capa: o Cadillac Racing V-Series.R de 2023 e o Chevrolet Corvette E-Ray de 2024. O jogo também apresenta 20 pistas reconstruídas com clima e tempo dinâmicos, arquibancadas vivas e pistas que se adaptam às mudanças de temperatura. Forza Motorsport promete ser uma experiência imersiva e realista, com gráficos impressionantes, jogabilidade dinâmica e física avançada. O jogo também aproveita os recursos do console Xbox Series X|S, como a Ray Tracing e os gráficos 4K ultra HD. Para saber mais sobre o jogo, visite o [site oficial](https://www.xbox.com/pt-BR/games/forza-motorsport) ou assista ao [trailer](https://www.youtube.com/watch?v=em4gv1Ietko).
&nbsp;
&nbsp;
&nbsp;
![](https://cdn.cloudflare.steamstatic.com/steam/apps/2440510/capsule_616x353.jpg?t=1696010427)