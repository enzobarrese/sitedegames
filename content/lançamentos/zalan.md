---
title: "Alan Wake 2 - 27 outubro"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Após o sucesso do primeiro jogo, Alan Wake 2, desenvolvido pela Remedy Entertainment, promete trazer mais suspense e terror para os fãs da saga. O jogo, que será lançado em 27 de outubro de 2023 para PC, PlayStation 5 e Xbox Series X|S, conta a história de dois heróis: Saga Anderson, uma investigadora que está atrás de um assassino em série que comete crimes rituais em uma vila isolada, e Alan Wake, um escritor que está preso em um lugar obscuro há 13 anos, onde seus pesadelos se tornam realidade. Os dois personagens terão que enfrentar as forças da escuridão que os cercam e descobrir o que os une. Alan Wake 2 promete ser uma experiência imersiva e assustadora, com gráficos impressionantes, jogabilidade dinâmica e uma trilha sonora envolvente. Quem comprar o jogo na pré-venda receberá itens digitais exclusivos, como um livro digital com a história de Alan Wake, um tema para o console e um pacote de armas especiais. Para saber mais sobre o jogo, visite o [site oficial](https://www.alanwake.com/pt/). Assista também o [trailer](https://www.youtube.com/watch?v=q0vNoRhuV_I) do jogo. 
&nbsp;
&nbsp;
&nbsp;
![](https://cdn1.epicgames.com/offer/c4763f236d08423eb47b4c3008779c84/EGS_AlanWake2_RemedyEntertainment_S1_2560x1440-ec44404c0b41bc457cb94cd72cf85872?h=270&quality=medium&resize=1&w=480)
