---
title: "League of Legends"
date: 2021-09-18T23:27:57-03:00
draft: false
---

&nbsp;

## Skins que homenageiam o Brasil

&nbsp;

O jogo é recheado de referências, mas essa é, sem dúvidas, uma das mais interessantes. As personagens Anívia, Gangplank, Hecarim e Nami têm skins baseadas na cultura brasileira.

&nbsp;

Carnanívia: referência a popular festa do país;

&nbsp;

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWuhMkbVS56iHh0jBXVJ3QPJPcw0DeeTq9-Mp-BAZ79sV-9RJECkQ-M_BWPX_o1m-SmXI&usqp=CAU)

&nbsp;

Gangplank Forças Especiais: a skin que o pirata utiliza é uma referência direta à força policial do Rio de Janeiro, o BOPE;

&nbsp;

![](https://i.ytimg.com/vi/MnZcqjBuRNA/sddefault.jpg)

&nbsp;

Hecarim Sabugueiro: uma referência direta à flora brasileira;

&nbsp;

![](https://i.ytimg.com/vi/s9cB0zIf7To/sddefault.jpg)

&nbsp;

Nami Iara: o folclore brasileiro está presente no jogo com essa skin.

&nbsp;

![](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/289e3a88-77b3-43d5-b8b0-7fce0c0cfa61/d88d959-98092061-d220-450e-9ef2-adf929efc785.jpg/v1/fill/w_1192,h_670,q_70,strp/league_of_legends__river_spirit_nami_by_nightfall1007_d88d959-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTA4MCIsInBhdGgiOiJcL2ZcLzI4OWUzYTg4LTc3YjMtNDNkNS1iOGIwLTdmY2UwYzBjZmE2MVwvZDg4ZDk1OS05ODA5MjA2MS1kMjIwLTQ1MGUtOWVmMi1hZGY5MjllZmM3ODUuanBnIiwid2lkdGgiOiI8PTE5MjAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.U-uIJLTxGmwbCJwvYWtg_Nppkr9Jg_5xEQNnRGgpCAI)

&nbsp;

## Skins mais raras de League of Legends

&nbsp;

Existem várias skins que, quem possui deve sentir orgulho devido a sua raridade, mas só quem adquiriu a Versão de Colecionador, versão paga do jogo vendida em lojas físicas em 2009, possuem as skins Kayle Prateada, Ryze Jovem, Annie Gótica e Alistar Negro.

&nbsp;

![]( https://www.ggrecon.com/media/ghafh5r3/lol-rarest-skins-early.jpg?mode=crop&width=682&quality=80&format=webp)

&nbsp;

Colocando em perspectiva, segundo o site PC Gamer em matéria do início de novembro, o jogo tem 180 milhões de jogadores ativos, mais do que a Steam que conta com 120. O número de pessoas que possuem a Versão de Colecionador, segundo o site Early Game, é de cerca de 65 mil pessoas.

