---
title: "Minecraft"
date: 2021-09-18T23:27:57-03:00
draft: false
---

&nbsp;

## Fatos do minecraft que com certeza voce não sabia

&nbsp;

### A primeira versão foi feita em apenas seis dias

&nbsp;

Primeira versão de Minecraft trazia apenas blocos de grama e pedregulho

&nbsp;

![](https://s2-techtudo.glbimg.com/IgAZZP5f8pUr5_r8b7Yl97SD0WE=/0x0:695x390/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2022/R/s/dAWj2VSIGeYkEQqPB7EQ/usa-basketball-8-.jpg)

&nbsp;

Para lançar sua companhia Mojang AB, o programador e designer sueco Markus "Notch" Persson preparou um jogo. O projeto tratava de um título gratuito estilo sandbox, ou seja, que oferece grande liberdade para os players, para levar os usuários através de uma exploração do mundo virtual. Os trabalhos se iniciaram em 10 de maio de 2009 e, seis dias depois, Notch fez os ajustes finais. A primeira versão de Minecraft foi lançada em 17 de maio de 2009 com blocos de grama e pedregulho.

&nbsp;

### Inicialmente o jogo tinha outro nome

&nbsp;

Minecraft já foi chamado de Cave Game e ainda conta com um nome alternativo.

&nbsp;

![](https://s2-techtudo.glbimg.com/mOabsLRFfYi9uo92XUIAPFoPX-8=/0x0:600x338/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2022/C/Z/AK0nwRQvit7vzyVIADBg/minceraft-600x338.jpg)

&nbsp;

Quando Notch iniciou o projeto de Minecraft, o jogo era chamado de Cave Game. A primeira mudança trouxe o seguinte título “Minecraft: Order of the Stone” (em português, Era da Pedra), que foi simplificado e se tornou apenas Minecraft. No entanto, até hoje o jogo possui um segundo nome. Um erro de ortografia aparece no menu principal em um a cada 10.000 plays e Minecraft vira “Minceraft”.

&nbsp;

### O som de Ghasts é de um gato dormindo

&nbsp;

Ghats se parecem com águas-vivas e produzem som de miado.

&nbsp;

![](https://s2-techtudo.glbimg.com/37i2gv0MzIA5054RGUaLyOuz_-M=/0x0:695x390/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2021/m/m/GC4qfYS7mTxWBDn2Wzpg/como-invocar-ender-dragon-minecraft-passo-3-ghast-nether.jpg)

&nbsp;

Um dos monstros de Minecraft foi dublado por um animal do mundo real. Os Ghasts vivem flutuando no Nhest, o inferno do jogo, e, quando atacam, eles atiram bolas de fogo e emitem o barulho agudo de um miado. O som é fruto de uma gravação acidental feita pelo produtor musical do jogo. Daniel “C418” Rosenfeld gravou seu gato acordando de uma soneca.

&nbsp;

### Os creepers nasceram como um erro

&nbsp;

Creepers irão evitar se aproximar do jogador quando seu gato estiver por perto em Minecraft

&nbsp;

![](https://s2-techtudo.glbimg.com/cnhyl6XMCXDbxzPxnGVuOSASEQA=/0x0:695x390/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2020/2/3/vZMAdqT96p4vAfa3bXlA/minecraft-como-domesticar-gato-passo-5-creeper.jpg)

&nbsp;

O processo de criação dos creepers, um dos monstros nativos de Minecraft, começou com a tentativa de fazer um porco, mas o desenvolvedor do game trocou altura por comprimento ao inserir o código. “Eu não tenho um modelo de programação, então apenas escrevo os códigos e acidentalmente os fiz altos ao invés de longos, e ficou aquela coisa alta com quatro pequenas patinhas”, disse Notch no documentário “Minecraft: A História da Mojang”, de 2012. Os creepers chegam silenciosamente para destruir as construções, mas têm medo dos ocelotes (uma espécie de gato).

&nbsp;

### Um projeto pretende replicar todo o planeta Terra em Minecraft

&nbsp;

O projeto colaborativo Build the Earth, criado pelo youtuber PippenFT, quer levar os jogadores de Minecraft em uma viagem pelo mundo, recriando no game desertos, oceanos, cidades e grandes maravilhas, como a Floresta Amazônica e o Monte Everest. Um bloco no jogo equivale a cerca de um metro no mundo real e o projeto aceita contribuições de jogadores de todos os países e culturas.

&nbsp;

![](https://i.ytimg.com/vi/c5uA0BMSKaU/maxresdefault.jpg)

&nbsp;


Para se ter comparação, o mundo do Minecraft é muito maior que o nosso planeta. É possível seguir em linha reta por cerca de 60 mil quilômetros, o que equivale ao tamanho de Netuno e quatro vezes o tamanho da Terra.